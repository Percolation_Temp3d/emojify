const hamburger = document.querySelector(".hamburger");
const navMenu = document.querySelector(".nav-menu");
const navLink = document.querySelectorAll(".nav-link");

hamburger.addEventListener("click", activateMobileMenu);
navLink.forEach(n => n.addEventListener("click", closeMobileMenu));

function activateMobileMenu() {
  hamburger.classList.toggle("active");
  navMenu.classList.toggle("active");
}

function closeMobileMenu() {
  hamburger.classList.remove("active");
  navMenu.classList.remove("active");
}
